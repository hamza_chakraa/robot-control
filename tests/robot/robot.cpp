#include <catch2/catch.hpp>

#include <umrob/robot.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1" />
    <link name="b2" />
    <link name="tcp" />
    <joint name="j1" type="revolute">
        <parent link="root"/>
        <child link="b1"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j2" type="revolute">
        <parent link="b1"/>
        <child link="b2"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j3" type="fixed">
        <parent link="b2"/>
        <child link="tcp"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
</robot>
)";

bool operator==(const umrob::Robot::JointsLimits& a,
                const umrob::Robot::JointsLimits& b) {
    return a.max_position.isApprox(b.max_position) and
           a.min_position.isApprox(b.min_position) and
           a.max_velocity.isApprox(b.max_velocity);
}

TEST_CASE("Robot") {
    auto model = umrob::RobotModel(planar_robot);
    auto robot = umrob::Robot(model);

    SECTION("Initialization") {
        REQUIRE(static_cast<size_t>(robot.joints().state.position.size()) ==
                model.degreesOfFreedom());
        REQUIRE(static_cast<size_t>(robot.joints().state.velocity.size()) ==
                model.degreesOfFreedom());
        REQUIRE(static_cast<size_t>(robot.joints().command.position.size()) ==
                model.degreesOfFreedom());
        REQUIRE(static_cast<size_t>(robot.joints().command.velocity.size()) ==
                model.degreesOfFreedom());

        REQUIRE(robot.joints().state.position.isZero());
        REQUIRE(robot.joints().state.velocity.isZero());
        REQUIRE(robot.joints().command.position.isZero());
        REQUIRE(robot.joints().command.velocity.isZero());

        umrob::Robot::JointsLimits expected_limits(model.degreesOfFreedom());
        expected_limits.max_position << 3.14159, 3.14159;
        expected_limits.min_position = -expected_limits.max_position;
        expected_limits.max_velocity.setOnes();
        REQUIRE(robot.joints().limits == expected_limits);
    }

    SECTION("Control points") {
        constexpr auto cp_name_ok = "tcp";
        constexpr auto cp_name_nok = "foo";
        const auto& crobot = robot;
        REQUIRE_THROWS(robot.controlPoint(cp_name_nok));
        REQUIRE_THROWS(crobot.controlPoint(cp_name_nok));
        REQUIRE_THROWS(crobot.controlPoint(cp_name_ok));
        REQUIRE_NOTHROW(robot.controlPoint(cp_name_ok));
        REQUIRE_NOTHROW(crobot.controlPoint(cp_name_ok));

        const auto& cp = robot.controlPoint(cp_name_ok);
        auto checkZero = [](const umrob::Robot::ControlPointData& cp) {
            REQUIRE(cp.position.matrix().isIdentity());
            REQUIRE(cp.position_vector.isZero());
            REQUIRE(cp.velocity.isZero());
        };
        checkZero(cp.state);
        checkZero(cp.target);
        checkZero(cp.command);
        cp.limits.max_velocity.isConstant(
            std::numeric_limits<double>::infinity());
    }
}