//! \file robot_controller.h
//! \author Benjamin Navarro
//! \brief Declaration of the RobotController class
//! \date 08-2020

#pragma once

#include <umrob/robot.h>
#include <umrob/robot_model.h>

#include <epigraph.hpp>

#include <string>
#include <map>
#include <memory>

namespace umrob {

//! \brief Optimization-based robot controller
//!
//! The controller relies on a weighted quadratic programming controller,
//! meaning that each task has its own weight. The higher the  weight, the
//! higher the priority the task has
//!
class RobotController {
public:
    //! \brief Construct a controller using a given robot and a time step
    //!
    //! \param robot The robot to control
    //! \param time_step The control time step in seconds
    RobotController(Robot& robot, double time_step);

    //! \brief Add a joints position constraint to the controller. Uses the
    //! joints.limits.{min,max}_position data to determine the bounds.
    //! The limits can be updated in real time
    //!
    void addJointsPositionConstraint();

    //! \brief Add a joints velocity constraint to the controller. Uses the
    //! joints.limits.max_velocity data to determine the bounds
    //! (symetric). The limits can be updated in real time
    //!
    void addJointsVelocityConstraint();

    //! \brief Add a control point velocity constraint to the controller. Uses
    //! the control_point.limits.max_velocity data to determine the bounds
    //! (symetric). The limits can be updated in real time
    //!
    //! \param control_point The name of the control point to constrain
    void addControlPointVelocityConstraint(const std::string& control_point);

    //! \brief Add a control point position task to the controller. The task is
    //! realized using a proportional controller using
    //! control_point.{target,state}.position_vector as inputs. The target can
    //! be updated in real time
    //!
    //! \param control_point The name of the control point to control
    //! \param weight The weight of the task
    //! \param gain The gain of the proportional controller
    void addControlPointPositionTask(const std::string& control_point,
                                     double weight, double gain);

    //! \brief Add a control point velocity task to the controller. The task is
    //! simply to follow control_point.target.velocity. The target can
    //! be updated in real time
    //!
    //! \param control_point The name of the control point to control
    //! \param weight The weight of the task
    void addControlPointVelocityTask(const std::string& control_point,
                                     double weight);

    //! \brief Set the robot commands using its current state
    //!
    void reset();

    //! \brief Use the current controller configuration and the robot's state to
    //! compute the commands. Requires the robot's model to be updated
    //! beforehand. This function updates:
    //!  - joints.command.{velocity,position}
    //!  - all control points command.{velocity,position,position_vector}
    //!
    //! \return bool True if a solution was found, false otherwise. If no
    //! solution was found, the robot commands are not updated.
    bool update();

    //! \brief Print to the standard output the current configuration of the
    //! controller
    //!
    void print() const;

    //! \brief Read/write access to the controlled robot
    //!
    //! \return Robot& The controlled robot
    Robot& robot() {
        return robot_;
    }

    //! \brief Read write access to the controlled robot's model
    //!
    //! \return Robot& The controlled robot
    RobotModel& model() {
        return robot().model();
    }

private:
    //! \brief Reset the internal OSQP solver. Must be called after each
    //! modification to the optimization problem qp_.
    //!
    void resetSolver();

    //! \brief Setup a task for the given control point. Must be called before
    //! accessing the task map tasks_ to make sure that everything is ready to
    //! use
    //!
    //! \param control_point Name of the control point to create a task for
    void setupTask(const std::string& control_point);

    //! \brief Hold data associated with a task: a Jacobian and its selection
    //! matrix
    //!
    //! The selection matrix selects from a complete joints velocity vector only
    //! the components that are involved in the task
    //!
    struct Task {
        Jacobian jacobian;
        Eigen::MatrixXd selection_matrix;
    };

    Robot& robot_;
    cvx::OptimizationProblem qp_;
    std::unique_ptr<cvx::osqp::OSQPSolver> solver_;
    std::map<std::string, Task> tasks_;
    cvx::MatrixX joints_velocity_;
    const double time_step_;
};

} // namespace umrob