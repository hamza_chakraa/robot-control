//! \file robot.h
//! \author Benjamin Navarro
//! \brief Declaration of the Robot class
//! \date 08-2020

#pragma once

#include <umrob/robot_model.h>

#include <Eigen/Dense>

#include <string>
#include <map>

namespace Eigen {
using Vector6d = Eigen::Matrix<double, 6, 1>;
}

namespace umrob {

//! \brief Store data associated with a robot: joint state, command, control
//! points, etc
//!
class Robot {
public:
    struct JointsData;
    struct JointsLimits;
    struct Joints;
    struct ControlPointData;
    struct ControlPointLimits;
    struct ControlPoint;

    //! \brief Construct a Robot using a model
    //!
    //! \param model The model matching the robot to create
    explicit Robot(RobotModel& model);

    //! \brief Read/write access to the joints
    //!
    //! \return Joints& The joints
    Joints& joints();

    //! \brief Read only access to the joints
    //!
    //! \return Joints& The joints
    const Joints& joints() const;

    //! \brief Read/write access to the given control point. Throws an exception
    //! if the model doesn't have a corresponding link
    //!
    //! \param name Name of the control point matching a link inside the model
    //! \return ControlPoint& The control point
    ControlPoint& controlPoint(const std::string& name);

    //! \brief Read only access to the given control point. Throws an exception
    //! if the non-const controlPoint() hasn't been called previously to create
    //! a control point
    //!
    //! \param name Name of the control point
    //! \return const ControlPoint& The control point
    const ControlPoint& controlPoint(const std::string& name) const;

    //! \brief Read/write access to the robot's model
    //!
    //! \return RobotModel& The model
    RobotModel& model();

    //! \brief Read only access to the robot's model
    //!
    //! \return RobotModel& The model
    const RobotModel& model() const;

    //! \brief Update all the the position_vector fields of all the existing
    //! control points
    //!
    //! see  updatePositionVector()
    void updatePositionVectors();

    //! \brief Update the position_vector field of a ControlPointData
    //!
    //! A position_vector is a 6 components vector associated whith an affine
    //! transformation. The first 3 components are (x,y,z) translations and the
    //! last 3 ones a compressed view of the angle-axis associated with the
    //! rotation matrix (angle * axis). Can be used for error computations and
    //! logging
    //!
    //! \param cp The ControlPointData to update
    void updatePositionVector(ControlPointData& cp);

    //! \brief Set robot's state from the model's state
    //!
    void setStateFromModel();

    //! \brief Set the model's state from the robot's state
    //!
    void updateModelFromState();

    //! \brief Udpdate the model with the current state, run the forward
    //! kinematics and use the results to update the robot's state
    //!
    void update();

    struct JointsData {
        explicit JointsData(size_t dofs);

        Eigen::VectorXd position;
        Eigen::VectorXd velocity;
    };

    struct JointsLimits {
        explicit JointsLimits(size_t dofs);

        Eigen::VectorXd min_position;
        Eigen::VectorXd max_position;
        Eigen::VectorXd max_velocity;
    };

    struct Joints {
        explicit Joints(size_t dofs);

        JointsData state;
        JointsData command;
        JointsLimits limits;
    };

    struct ControlPointData {
        ControlPointData();

        Eigen::Affine3d position;
        Eigen::Vector6d position_vector;
        Eigen::Vector6d velocity;
    };

    struct ControlPointLimits {
        ControlPointLimits();

        Eigen::Vector6d max_velocity;
    };

    struct ControlPoint {
        ControlPointData state;
        ControlPointData target;
        ControlPointData command;
        ControlPointLimits limits;
    };

private:
    //! \brief Set the joints limits according to the robot's model
    //!
    void setJointsLimitsFromModel();

    Joints joints_;
    std::map<std::string, ControlPoint> control_points_;
    RobotModel& model_;
};

} // namespace umrob