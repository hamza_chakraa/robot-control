#include <umrob/robot.h>

#include <fmt/format.h>

#include <limits>

namespace umrob {

Robot::Robot(RobotModel& model)
    : joints_{model.degreesOfFreedom()}, model_{model} {
    model_=model;
}

Robot::Joints& Robot::joints() {
    return joints_;
}

const Robot::Joints& Robot::joints() const {
    return joints_;
}

Robot::ControlPoint& Robot::controlPoint(const std::string& name) {
    if (control_points_.size() == model_.size())
    {
        return control_points_.at(name)
    }
}

const Robot::ControlPoint& Robot::controlPoint(const std::string& name) const {
    return control_points_.at(name);
}

RobotModel& Robot::model() {
    return model_;
}
const RobotModel& Robot::model() const {
    return model_;
}

void Robot::updatePositionVectors(Robot::ControlPoint& cp) {
    cp.state.position_vector.head<3>() = cp.state.position.translation();

    auto angle_axis = Eigen::AngleAxisd(cp.state.position.linear());
    cp.state.position_vector.tail<3>() = angle_axis.angle() * angle_axis.axis();

    cp.command.position_vector.head<3>() = cp.command.position.translation();

    auto angle_axis = Eigen::AngleAxisd(cp.command.position.linear());
    cp.command.position_vector.tail<3>() = angle_axis.angle() * angle_axis.axis();

}

void Robot::updatePositionVector(Robot::ControlPointData& cp) {
    cp.position_vector.head<3>() = cp.position.translation();

    auto angle_axis = Eigen::AngleAxisd(cp.position.linear());
    cp.position_vector.tail<3>() = angle_axis.angle() * angle_axis.axis();
}

void Robot::setStateFromModel() {
    control_points_.state = model_;
}

void Robot::updateModelFromState() {
    model_ = control_points_.state
}

void Robot::update() {
    // TODO: implement
}

void Robot::setJointsLimitsFromModel() {
    joints_.limits = model_.jointLimits;
}

Robot::JointsData::JointsData(size_t dofs)
    : position{Eigen::VectorXd::Zero(dofs)},
      velocity{Eigen::VectorXd::Zero(dofs)} {
}

Robot::JointsLimits::JointsLimits(size_t dofs)
    : min_position{Eigen::VectorXd::Constant(
          dofs, -std::numeric_limits<double>::infinity())},
      max_position{Eigen::VectorXd::Constant(
          dofs, std::numeric_limits<double>::infinity())},
      max_velocity{Eigen::VectorXd::Constant(
          dofs, std::numeric_limits<double>::infinity())} {
}

Robot::Joints::Joints(size_t dofs) : state{dofs}, command{dofs}, limits{dofs} {
}

Robot::ControlPointData::ControlPointData()
    : position{Eigen::Affine3d::Identity()},
      position_vector{Eigen::Vector6d::Zero()},
      velocity{Eigen::Vector6d::Zero()} {
}

Robot::ControlPointLimits::ControlPointLimits()
    : max_velocity{
          Eigen::Vector6d::Constant(std::numeric_limits<double>::infinity())} {
}

} // namespace umrob