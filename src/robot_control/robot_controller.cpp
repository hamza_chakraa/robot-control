#include <umrob/robot_controller.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace umrob {

RobotController::RobotController(Robot& robot, double time_step)
    : robot_{robot}, time_step_{time_step} {
    robot_=robot;
    time_step_=time_step;
}

void RobotController::addJointsPositionConstraint() {
        qp_.addConstraint(cvx::box(
        -cvx::dynpar(robot_.joints().limits.max_position), joints_position_,
        cvx::dynpar(robot_.joints().limits.max_position)));

    resetSolver();
}

void RobotController::addJointsVelocityConstraint() {
    qp_.addConstraint(cvx::box(
        -cvx::dynpar(robot_.joints().limits.max_velocity), joints_velocity_,
        cvx::dynpar(robot_.joints().limits.max_velocity)));

    resetSolver();
}

void RobotController::addControlPointVelocityConstraint([
    const std::string& control_point) {
    // TODO: implement
}

void RobotController::addControlPointPositionTask(
    const std::string& control_point,
    double weight, double gain) {
    // TODO: implement
}

void RobotController::addControlPointVelocityTask(
    const std::string& control_point,
    double weight) {
    setupTask(control_point);
    auto& task = tasks_.at(control_point);
    auto& cp = robot_.controlPoint(control_point);

    // Minimizes || weight * (J*S*q_dot - x_dot) ||
    qp_.addCostTerm((cvx::par(weight) *
                     (cvx::dynpar(task.jacobian.matrix) *
                          cvx::par(task.selection_matrix) * joints_velocity_ -
                      cvx::dynpar(cp.target.velocity)))
                        .squaredNorm());

    resetSolver();
}

void RobotController::setupTask([
    const std::string& control_point) {
    auto& task = tasks_[control_point];
    if (task.jacobian.matrix.size() == 0) {
        task.jacobian = model().linkJacobian(control_point);
        auto& jacobian = task.jacobian;

        // This selection matrix extracts the joint velocities
        // involved for this Jacobian and discards the others
        task.selection_matrix.resize(jacobian.matrix.cols(),
                                     joints_velocity_.rows());

        task.selection_matrix.setZero();
        for (size_t i = 0; i < jacobian.joints_name.size(); i++) {
            task.selection_matrix(
                i, model().jointIndex(jacobian.joints_name[i])) = 1;
        }
    }
}

void RobotController::reset() {
    // TODO: implement
}

bool RobotController::update() {
    if (not solver_) {
        throw std::runtime_error("You must setup you control problem before "
                                 "calling RobotController::update()");
    }

    // Step 1: update task Jacobians
    // TODO: implement

    // Step 2: solve the problem
    // TODO: implement

    // Step 3: update the joints command vector using the solution
    Eigen::MatrixXd joints_velocity_sol = cvx::eval(joints_velocity_);
    // TODO: implement

    // Step 4: update the control points commands
    for (auto& [name, task] : tasks_) {
        auto& cp = robot().controlPoint(name);

        // TODO: implement
        // cp.command.velocity = ...;

        cp.command.position.translation() =
            cp.state.position.translation() +
            cp.command.velocity.head<3>() * time_step_;
        cp.command.position.linear() =
            static_cast<Eigen::Quaterniond>(cp.state.position.linear())
                .integrate(cp.command.velocity.tail<3>(), time_step_)
                .toRotationMatrix();

        robot_.updatePositionVector(cp.command);
    }

    return true;
}

void RobotController::print() const {
    fmt::print("{}", qp_);
}

void RobotController::resetSolver() {
    solver_ = nullptr;
    solver_ = std::make_unique<cvx::osqp::OSQPSolver>(qp_);
    solver_->setAlpha(1.);
    solver_->setEpsAbs(1e-6);
    solver_->setEpsRel(1e-6);
}

} // namespace umrob