#include <umrob/robot.h>
#include <umrob/robot_model.h>
#include <umrob/robot_controller.h>

#include <fmt/format.h>
#include <fmt/ostream.h>

constexpr auto planar_robot =
    R"(<robot name="PlanarRobot">
    <link name="root" />
    <link name="b1" />
    <link name="b2" />
    <link name="tcp" />
    <joint name="j1" type="revolute">
        <parent link="root"/>
        <child link="b1"/>
        <origin rpy="0 0 0" xyz="0 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j2" type="revolute">
        <parent link="b1"/>
        <child link="b2"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
        <axis xyz="0 0 1"/>
        <limit effort="10" lower="-3.14159" upper="3.14159" velocity="1"/>
    </joint>
    <joint name="j3" type="fixed">
        <parent link="b2"/>
        <child link="tcp"/>
        <origin rpy="0 0 0" xyz="0.5 0 0"/>
    </joint>
</robot>
)";

int main(int argc, const char* argv[]) {
    constexpr double time_step = 0.01;
    auto model = umrob::RobotModel(planar_robot);
    auto robot = umrob::Robot(model);
    auto controller = umrob::RobotController(robot, time_step);

    const Eigen::Index j1_idx = model.jointIndex("j1");
    const Eigen::Index j2_idx = model.jointIndex("j2");
    if (argc > 1) {
        robot.joints().state.position[j1_idx] = atof(argv[1]);
    } else {
        robot.joints().state.position[j1_idx] = M_PI / 2.;
    }
    if (argc > 2) {
        robot.joints().state.position[j2_idx] = atof(argv[2]);
    } else {
        robot.joints().state.position[j2_idx] = M_PI / 2.;
    }

    fmt::print("Joint positions:\n");
    for (auto it : model.jointsPosition()) {
        fmt::print("{}: {}\n", it.first, it.second);
    }

    auto& cp = robot.controlPoint("tcp");
    cp.target.velocity.x() = 0.1;

    controller.addControlPointVelocityTask("tcp", 1.);
    controller.addJointsVelocityConstraint();

    controller.print();

    controller.reset();

    for (size_t i = 0; i < 1. / time_step; i++) {
        robot.update();

        if (controller.update()) {

            fmt::print("joint vel: {}\n",
                       robot.joints().command.velocity.transpose());
            fmt::print("joint pos: {}\n",
                       robot.joints().command.position.transpose());
            fmt::print("tcp pos: {}\n", cp.state.position_vector.transpose());
            fmt::print("tcp vel: {}\n", cp.state.velocity.transpose());

            robot.joints().state = robot.joints().command;
            cp.state = cp.command;
        } else {
            fmt::print(stderr, "Failed to compute the robot command\n");
            std::exit(-1);
        }
    }
}